//
//  Constants.swift
//  TwitterTutorial
//
//  Created by Rahman Pratama on 10/5/22.
//

import Firebase

let DB_REF = Database.database(url: "https://twittertutorial-c6731-default-rtdb.asia-southeast1.firebasedatabase.app/").reference()

let REF_USERS =  DB_REF.child("users")
