//
//  FeedController.swift
//  TwitterTutorial
//
//  Created by Rahman Pratama on 9/30/22.
//

import UIKit

class FeedController: UIViewController {
    
    // Properties
    
    // Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        
    }
    
    // Helpers
    
    func configureUI(){
        view.backgroundColor = .systemRed
        
        let imageView = UIImageView(image: UIImage(named: "twitter_logo_blue"))
        imageView.contentMode = .scaleAspectFit
        navigationItem.titleView = imageView
    }
        
}
