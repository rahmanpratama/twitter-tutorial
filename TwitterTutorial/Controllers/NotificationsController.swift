//
//  NotificationsController.swift
//  TwitterTutorial
//
//  Created by Rahman Pratama on 9/30/22.
//

import UIKit

class NotificationsController: UIViewController {
    
    // Properties
    
    // Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        configureUI()
    }
    
    // Helpers
    func configureUI(){
        view.backgroundColor = .systemPink
        
        let imageView = UIImageView(image: UIImage(named: "twitter_logo_blue"))
        imageView.contentMode = .scaleAspectFit
        navigationItem.title = "Notifications"
    }
}

