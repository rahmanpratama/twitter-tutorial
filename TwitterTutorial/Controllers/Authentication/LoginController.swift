//
// Properties


// Lifecycle


// Selector

// Helpers

//  LoginController.swift
//  TwitterTutorial
//
//  Created by Rahman Pratama on 10/3/22.
//

import UIKit

class LoginController: UIViewController{
    


// Properties
    private let logoImageView: UIImageView = {
        let logo =  UIImageView()
        logo.contentMode = .scaleAspectFit
        logo.clipsToBounds = true
        logo.image = UIImage(named:"TwitterLogo" )
        return logo
    }()
    
    private lazy var emailContainerView: UIView = {
        
        let image = UIImage(imageLiteralResourceName: "ic_mail_outline_white_2x-1")
        let view = Utilities().inputContainerView(withImage: image, textField: emailTextField )
        
        
        return view
        
    }()
    
    private lazy var passwordContainerView: UIView = {
        
        let image = UIImage(imageLiteralResourceName: "ic_lock_outline_white_2x")
        let view = Utilities().inputContainerView(withImage: image, textField: passwordTextField)
        
        return view
        
    }()
    
    private let emailTextField: UITextField = {
        
        let textfield = Utilities().textField(withPlaceHolder: "Email")
        textfield.isSecureTextEntry = false
        return textfield
        
    }()
    
    private let passwordTextField: UITextField = {
        
        let textfield = Utilities().textField(withPlaceHolder: "Password")
        textfield.isSecureTextEntry = true
        return textfield
        
    }()
    
    private let loginButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Login", for: .normal)
        button.setTitleColor(.twitterBlue, for: .normal)
        button.backgroundColor = .white
        button.heightAnchor.constraint(equalToConstant: 50).isActive = true
        button.layer.cornerRadius = 5
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        button.addTarget(self , action: #selector(handleLogin), for: .touchUpInside)
        return button
        
        
    }()
    
    private let dontHaveAccountButton: UIButton = {
        let button = Utilities().attributedButton("Don't have an account ?", " Sign Up")
        button.addTarget(self , action: #selector(handleShowSignUp), for: .touchUpInside)
        return button
    }()
// Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
}

// Selector
    @objc func handleLogin(){
        print("loginnnnnnn")
    }
     
    @objc func handleShowSignUp(){
        let controller = RegistrationController()
        navigationController?.pushViewController(controller, animated: true)
    }
// Helpers
    
    func configureUI(){
        view.backgroundColor = .twitterBlue
        navigationController?.navigationBar.barStyle = .black
        navigationController?.navigationBar.isHidden = true
        
        view.addSubview(logoImageView)
        logoImageView.centerX(inView: view, topAnchor: view.safeAreaLayoutGuide.topAnchor)
        logoImageView.setDimensions(width: 150, height: 150)

        let stack = UIStackView(arrangedSubviews: [emailContainerView, passwordContainerView, loginButton])
        stack.axis = .vertical
        stack.spacing = 20
        stack.distribution = .fillEqually
        
        view.addSubview(stack)
        stack.anchor(top: logoImageView.bottomAnchor, left: view.leftAnchor, right: view.rightAnchor, paddingLeft: 32, paddingRight: 32)
        
        
        view.addSubview(dontHaveAccountButton)
        dontHaveAccountButton.anchor(left: view.leftAnchor,
                                     bottom:view.safeAreaLayoutGuide.bottomAnchor,
                                     right: view.rightAnchor,
                                     paddingLeft: 40,
                                     paddingBottom: 16,
                                     paddingRight: 40)
                                     
        
    }
    
    
    
}



