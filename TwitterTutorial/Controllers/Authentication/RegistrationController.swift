//
//  RegistrationController.swift
//  TwitterTutorial
//
//  Created by Rahman Pratama on 10/4/22.
//

import UIKit
import Firebase

class RegistrationController: UIViewController{

    // MARK : - Properties
    
    private let imagePicker = UIImagePickerController()
    private var profileImage : UIImage?
    
    private let plusPhotoImageView: UIButton = {
        let button =  UIButton(type: .system)
        button.setImage(UIImage(named: "plus_photo"), for: .normal)
        button.tintColor = .white
        button.addTarget(self , action: #selector(handleAddProfilePhoto), for: .touchUpInside)
        return button
    }()
    
    private let alreadyHaveAccount : UIButton = {
        let button = Utilities().attributedButton("Don't have an account ?", "  Login")
        button.addTarget(self , action: #selector(handleShowLogin), for: .touchUpInside)
        return button
    }()
    
    private lazy var emailContainerView: UIView = {
        
        let image = UIImage(imageLiteralResourceName: "ic_mail_outline_white_2x-1")
        let view = Utilities().inputContainerView(withImage: image, textField: emailTextField )
        
        
        return view
        
    }()
    
    private lazy var passwordContainerView: UIView = {
        
        let image = UIImage(imageLiteralResourceName: "ic_lock_outline_white_2x")
        let view = Utilities().inputContainerView(withImage: image, textField: passwordTextField)
        
        return view
        
    }()
    
    private lazy var fullnameContainerView: UIView = {
        
        let image = UIImage(imageLiteralResourceName: "ic_mail_outline_white_2x-1")
        let view = Utilities().inputContainerView(withImage: image, textField: fullnameTextField )
        
        
        return view
        
    }()
    
    private lazy var usernameContainerView: UIView = {
        
        let image = UIImage(imageLiteralResourceName: "ic_lock_outline_white_2x")
        let view = Utilities().inputContainerView(withImage: image, textField: usernameTextField)
        
        return view
        
    }()
    
    private let emailTextField: UITextField = {
        
        let textfield = Utilities().textField(withPlaceHolder: "Email")
        textfield.isSecureTextEntry = false
        return textfield
        
    }()
    
    private let passwordTextField: UITextField = {
        
        let textfield = Utilities().textField(withPlaceHolder: "Password")
        textfield.isSecureTextEntry = true
        return textfield
        
    }()
    
    private let fullnameTextField: UITextField = {
        
        let textfield = Utilities().textField(withPlaceHolder: "Fullname")
        textfield.isSecureTextEntry = false
        return textfield
        
    }()
    
    private let usernameTextField: UITextField = {
        
        let textfield = Utilities().textField(withPlaceHolder: "Username")
        textfield.isSecureTextEntry = false
        return textfield
        
    }()
    
    private let registerButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Sign Up", for: .normal)
        button.setTitleColor(.twitterBlue, for: .normal)
        button.backgroundColor = .white
        button.heightAnchor.constraint(equalToConstant: 50).isActive = true
        button.layer.cornerRadius = 5
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        button.addTarget(self , action: #selector(handleRegistation), for: .touchUpInside)
        return button
    }()
    // Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        
    }
    
    // Selector
    
    @objc func handleShowLogin(){
        navigationController?.popViewController(animated: true)
    }
    
    @objc func handleAddProfilePhoto(){
        present(imagePicker, animated: true, completion: nil)

    }
    
    @objc func handleRegistation(){
        guard let profileImage = profileImage else{
            print ("DEBUG : Please Select your Profile Picture" )
            return
        }
        
        guard let email = emailTextField.text else {return}
        guard let password = passwordTextField.text else {return}
        guard let fullname = fullnameTextField.text else {return}
        guard let username = usernameTextField.text else {return}
       
        Auth.auth().createUser(withEmail: email, password: password){
            (result, error) in
            
            if let error = error {
                print ("DEBUG : Error is \(error.localizedDescription)" )
                return
            }
            guard let uid = result?.user.uid else { return }
            
            let values = ["email": email, "password" : password, "fullname": fullname]
            
            let ref = Database.database(url: "https://twittertutorial-c6731-default-rtdb.asia-southeast1.firebasedatabase.app/").reference().child("users").child(uid)
            
            ref.updateChildValues(values) { (error, ref) in
                print("DEBUG : Successfully Updated User Informations" )
                
                
            }
            
        }
        
    }
    
    
    
    
    // Helpers
    
    func configureUI(){
        view.backgroundColor = .twitterBlue
    
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        
        view.addSubview(plusPhotoImageView)
        plusPhotoImageView.centerX(inView: view, topAnchor: view.safeAreaLayoutGuide.topAnchor)
        plusPhotoImageView.setDimensions(width: 128, height: 128)

        let stack = UIStackView(arrangedSubviews: [emailContainerView, passwordContainerView, fullnameContainerView, usernameContainerView, registerButton])
        stack.axis = .vertical
        stack.spacing = 20
        stack.distribution = .fillEqually
        
        view.addSubview(stack)
        stack.anchor(top: plusPhotoImageView.bottomAnchor, left: view.leftAnchor, right: view.rightAnchor, paddingLeft: 32, paddingRight: 32)
        
        
        
        view.addSubview(alreadyHaveAccount)
        alreadyHaveAccount.anchor(left: view.leftAnchor,
                                     bottom:view.safeAreaLayoutGuide.bottomAnchor,
                                     right: view.rightAnchor,
                                     paddingLeft: 40,
                                     paddingBottom: 16,
                                     paddingRight: 40)
    }
    
    

    
}

extension RegistrationController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let profileImage = info[.editedImage] as? UIImage else {return}
        self.profileImage = profileImage
        plusPhotoImageView.layer.cornerRadius = 128/2
        plusPhotoImageView.layer.masksToBounds = true
        plusPhotoImageView.layer.borderColor = UIColor.white.cgColor
        plusPhotoImageView.layer.borderWidth = 2
        plusPhotoImageView.imageView?.contentMode = .scaleAspectFill
        plusPhotoImageView.imageView?.clipsToBounds = true
        
        self.plusPhotoImageView.setImage(profileImage.withRenderingMode(.alwaysOriginal), for: .normal)
        
        dismiss(animated: true, completion: nil)
    }
}
